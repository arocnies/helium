package communication;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by Aaron Nies on 6/8/2015.
 * Copyright 2015 all rights reserved.
 */
public interface Transport<T extends Serializable> extends Inbox, Outbox<T>
{
    /**
     * Sends an object of type T and returns the response.
     * This method can be expected to block the calling thread.
     *
     * @param t Serializable object to be sent.
     * @return Response to sent object.
     * @throws IOException
     */
    T submit(T t) throws IOException; // TODO: Don't require return type be the same as sent type. ALSO Don't use same T as the transport's T.
}
