package communication;

import event.Listenable;

/**
 * Created by Aaron Nies on 6/8/2015.
 * Copyright 2015 all rights reserved.
 */
public interface Inbox extends Listenable
{
    boolean start();

    void stop();
}
