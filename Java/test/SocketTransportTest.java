import communication.tcp.SocketTransport;
import event.Listener;
import event.Responder;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;

/**
 * Created by Aaron Nies on 6/9/2015.
 * Copyright 2015 all rights reserved.
 */
public class SocketTransportTest
{
    public static void main(String[] args)
    {
        Executors.newSingleThreadExecutor().execute(SocketTransportTest::runServer);
        runClient();
    }

    private static void runClient()
    {
        String columnSpace = "\t\t\t\t\t\t\t\t\t\t\t\t";
        try
        {
            SocketTransport<String> st = new SocketTransport<>(new Socket("localhost", 5555));
            System.out.println(columnSpace + "CLIENT connected.");
            st.addListener(new Listener<String>()
            {
                @Override
                public void trigger(String s)
                {
                    System.out.println(columnSpace + "CLIENT got: " + s);
                }
            });
            System.out.println(columnSpace + "CLIENT Added listener.");

            st.addResponder(new Responder<String, String>()
            {
                @Override
                public String respond(String s)
                {
                    System.out.println(columnSpace + "CLIENT RESPONDING!");
                    return (new StringBuilder(s).reverse()).toString();
                }
            });
            System.out.println(columnSpace + "CLIENT Added responder.");

            st.start();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private static void runServer()
    {
        try
        {
            ServerSocket serverSocket = new ServerSocket(5555);
            System.out.println("SERVER Started.");
            SocketTransport<String> st = new SocketTransport<>(serverSocket.accept());
            System.out.println("SERVER Accepted client.");

            st.addListener(new Listener<String>()
            {
                @Override
                public void trigger(String s)
                {
                    System.out.println("SERVER got: " + s);
                }
            });
            System.out.println("SERVER Added listener.");
            st.start();

            System.out.println("SUBMIT 'Hello': " + st.submit("Hello"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
