package communication.tcp;

import communication.HeliumPackage;
import communication.Outbox;
import io.sockets.ObjectIOSocket;

import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;

/**
 * Created by Aaron Nies on 6/8/2015.
 * Copyright 2015 all rights reserved.
 */
public class SocketOutbox<T extends Serializable> implements Outbox<T>
{
    // --- Vars ---

    private final ObjectIOSocket _socketToInbox; // Receiving is done through this socket.

    // --- Constructor ---

    /**
     * Constructs a new SocketInbox with a socket to its destination, usually an Inbox. Receiving is done through
     * this socket.
     *
     * @param socket Socket to source
     */
    public SocketOutbox(Socket socket)
    {
        if (socket == null)
        {
            throw new IllegalArgumentException();
        }
        _socketToInbox = new ObjectIOSocket(socket);
    }

    /**
     * Sends an object through the socket.
     *
     * @param t Serializable object to be sent
     * @throws IOException
     */
    @Override
    public void send(T t) throws IOException
    {
        send(new HeliumPackage<>(t));
    }

    /**
     * Sends a HeliumPackage over the socket. This internal method is called by its public counterpart.
     *
     * @param hePackage HeliumPackage to send
     * @throws IOException if writing to its socket fails
     */
    protected void send(HeliumPackage hePackage) throws IOException
    {
        _socketToInbox.writeObject(hePackage);
    }
}
