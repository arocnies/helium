package communication;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by Aaron on 5/21/2015.
 * Copyright 2015 all rights reserved.
 */

/**
 * An Outbox is an object capable of sending objects that extend serializable.
 * <p>
 * If you want to send and receive objects, use the Transport interface.
 * @param <T> Type of object being transported.
 */
public interface Outbox<T extends Serializable>
{
    /**
     * Sends an object of type T.
     * This method should not block the calling thread once the message is sent.
     *
     * @param t Serializable object to be sent.
     * @throws IOException
     */
    void send(T t) throws IOException;
}
