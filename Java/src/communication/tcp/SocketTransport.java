package communication.tcp;

import communication.HeliumPackage;
import communication.Transport;
import communication.event.HeliumResponseListener;
import event.*;
import io.sockets.ObjectIOSocket;

import java.io.Closeable;
import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;

/**
 * Created by Aaron Nies on 5/20/2015.
 * Copyright 2015 all rights reserved.
 */
public class SocketTransport<T extends Serializable> implements Transport<T>, Listenable, Respondable, Closeable
{

    // --- Vars ---

    private final ObjectIOSocket _socket; // Sending and receiving is done through this socket.
    private final SocketOutbox<T> _outbox;
    private final SocketInbox _inbox;
    private final Listenable _listenable;
    private final Respondable _respondable;

    // --- Constructor ---

    /**
     * Constructs a new SocketTransport with a socket to its client. Sending and receiving is done through this socket.
     *
     * @param socket Socket to client
     */
    public SocketTransport(Socket socket)
    {
        if (socket == null)
        {
            throw new IllegalArgumentException();
        }
        _socket = new ObjectIOSocket(socket);
        _outbox = new SocketOutbox<>(socket);
        _inbox = new SocketInbox(socket);
        _listenable = new ConcurrentListenable();
        _respondable = new ConcurrentRespondable();

        // Setup inbox listener for HeliumPackages.
        _inbox.addListener(new Listener<HeliumPackage<? extends Serializable>>()
        {
            @Override
            public void trigger(HeliumPackage<? extends Serializable> hePackage)
            {
                receivedPackage(hePackage);
            }
        });
    }

    // --- Methods public ---

    /**
     * This starts reading incoming objects from the socket. Use listeners to act or block on incoming objects. Once
     * started, the SocketTransport may be stopped with stop().
     *
     * @return True if the SocketTransport successfully started
     */
    @Override
    public boolean start()
    {
        return _inbox.start();
    }

    /**
     * This stops incoming objects from being read from the socket. Once stopped, the SocketTransport may be
     * restarted using start().
     */
    @Override
    public void stop()
    {
        _inbox.stop();
    }

    /**
     * Stops the transport and closes the socket. Once closed, the SocketTransport is no longer available for use.
     *
     * @throws IOException
     */
    @Override
    public void close() throws IOException
    {
        stop();
        _socket.close();
    }

    /**
     * Sends an object through the socket.
     *
     * @param t Serializable object to be sent
     * @throws IOException
     */
    @Override
    public void send(T t) throws IOException
    {
        _outbox.send(t);
    }

    /**
     * Sends an object and returns the response.
     *
     * @param t Serializable object to be sent.
     * @return The object response
     * @throws IOException
     */
    @Override
    public T submit(T t) throws IOException
    {
        HeliumPackage<T> heMessage = new HeliumPackage<>(t, null, true, false);
        HeliumPackage<T> response = submit(heMessage);
        return response == null ? null : response.getContents();
    }

    // --- Methods private ---

    /**
     * Called when a HeliumPackage arrives. This method unpacks the contents of the package and triggers the
     * listeners of the transport.
     *
     * @param hePackage Incoming object
     */
    private void receivedPackage(HeliumPackage<? extends Serializable> hePackage)
    {
        // If package is request.
        if (hePackage.isRequest())
        {
            // Trigger processor.
            receivedSubmission(hePackage);
        }
        // Trigger listeners.
        triggerListeners(hePackage.getContents());
    }

    private void receivedSubmission(HeliumPackage<? extends Serializable> hePackage)
    {
        try
        {
            Serializable responseContents = (Serializable) _respondable.triggerResponders(hePackage.getContents());
            HeliumPackage<Serializable> response = new HeliumPackage<>(responseContents, hePackage, false, true);
            _outbox.send(response);
        }
        catch (IOException e)
        {
            e.printStackTrace(); // Failed to send response. Contents may not have been serializable.
        }
    }

    /**
     * Sends a HeliumPackage and returns the response. Blocking is done by a temporary HeliumResponseListener. This
     * internal method is called by its public counterpart.
     *
     * @param request The HeliumPackage to send and wait for its response
     * @return The response HeliumPackage
     * @throws IOException
     */
    private HeliumPackage<T> submit(HeliumPackage<T> request) throws IOException
    {
        // Setup listener.
        HeliumPackage<T> response = null;
        HeliumResponseListener<T> hrl = new HeliumResponseListener<>(request);
        this.addListener(hrl);

        // Send message. Not a race condition because HeliumResponseListener will cache the response.
        _outbox.send(request);
        try
        {
            response = hrl.block();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace(); // Timeout during waiting for message.
        }
        this.removeListener(hrl);

        // Return response (allowed to return null);
        return response;
    }

    // --- Listenable overrides ---

    @Override
    public boolean addListener(Listener listener)
    {
        return _listenable.addListener(listener);
    }

    @Override
    public boolean removeListener(Listener listener)
    {
        return _listenable.removeListener(listener);
    }

    @Override
    public void triggerListeners(Object o)
    {
        _listenable.triggerListeners(o);
    }

    // --- Respondable overrides ---

    @Override
    public boolean addResponder(Responder responder)
    {
        return _respondable.addResponder(responder);
    }

    @Override
    public boolean removeResponder(Responder responder)
    {
        return _respondable.removeResponder(responder);
    }

    @Override
    public Object triggerResponders(Object t)
    {
        return _respondable.triggerResponders(t);
    }
}
