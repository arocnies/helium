package communication.tcp;

import communication.Inbox;
import event.ConcurrentListenable;
import io.sockets.ObjectIOSocket;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Aaron Nies on 6/8/2015.
 * Copyright 2015 all rights reserved.
 */
public class SocketInbox extends ConcurrentListenable implements Inbox
{

    // --- Vars ---

    private final ObjectIOSocket _socketToOutbox; // Receiving is done through this socket.
    private final ExecutorService _executorService; // ExecutorService for running the input thread.
    private Future _readFuture; // A future associated with the reading Runnable.

    // --- Constructor ---

    /**
     * Constructs a new SocketInbox with a socket to its source, usually an Outbox. Receiving is done through this
     * socket.
     *
     * @param socket Socket to source
     */
    public SocketInbox(Socket socket)
    {
        if (socket == null)
        {
            throw new IllegalArgumentException();
        }
        _socketToOutbox = new ObjectIOSocket(socket);
        _executorService = Executors.newSingleThreadExecutor();
    }

    // --- Methods public ---

    /**
     * This starts reading incoming objects from the socket. Use listeners to act or block on incoming objects. Once
     * started, the SocketInbox may be stopped with stop().
     *
     * @return True if the SocketInbox successfully started
     */
    public boolean start()
    {
        if (!isStarted()) // Only allow one thread to be running the main loop (readSocket method).
        {
            _readFuture = _executorService.submit(() -> {
                //noinspection InfiniteLoopStatement
                while (true) readSocket(); // readSocket can only be expected to finish if interrupted.
            });
        }
        return isStarted();
    }

    /**
     * This stops incoming objects from being read from the socket. Once stopped, the SocketInbox may be
     * restarted using start().
     */
    public void stop()
    {
        _readFuture.cancel(true);
    }

    // --- Methods private ---

    private boolean isStarted()
    {
        return _readFuture != null && !_readFuture.isDone();
    }

    /**
     * Reads incoming objects from the socket and passes them to receivedObject().
     */
    private void readSocket() throws IOException
    {
        try
        {
            Object readObj = _socketToOutbox.readObject();
            if (readObj != null)
            {
                triggerListeners(readObj);
            }
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace(); // Bad incoming data.
        }
    }
}
