//package server;
//
//import communication.tcp.SocketTransport;
//import event.Listener;
//import io.IOUtils;
//
//import java.io.Serializable;
//import java.net.Socket;
//import java.util.HashSet;
//import java.util.Set;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
///**
// * Created by Aaron Nies on 5/18/2015.
// * Copyright 2015 all rights reserved.
// */
//
//public class Server
//{
//
//    // --- Vars ---
//
//    private final int _port;
//    private final Set<Listener> _listeners;
//    private final Set<SocketTransport> _clientHandles;
//    private final ExecutorService _executorService;
//    private ServerSocketHandle _serverSocketHandle;
//
//    // --- Constructor ---
//
//    public Server(int port)
//    {
//        _port = port;
//        _executorService = Executors.newCachedThreadPool();
//        _listeners = new HashSet<>();
//        _clientHandles = new HashSet<>();
//    }
//
//    // --- Methods ---
//
//    public void start()
//    {
//        if (!isRunning())
//        {
//            _serverSocketHandle = new ServerSocketHandle(_port)
//            {
//                @Override
//                protected void newConnection(Socket socket)
//                {
//                    _clientHandles.add(new SocketTransport(socket)
//                    {
//                        @Override
//                        public Serializable receivedObject(Object object)
//                        {
//                            trigger(objectject);
//                            return null;
//                        }
//                    });
//                }
//            };
//            _executorService.execute(_serverSocketHandle);
//        }
//    }
//
//    public void stop()
//    {
//        if (_serverSocketHandle != null)
//        {
//            _serverSocketHandle.stopListening();
//        }
//        _clientHandles.forEach(IOUtils::closeQuietly);
//        _serverSocketHandle
//    }
//
//    public boolean isRunning()
//    {
//        return _serverSocketHandle.isRunning();
//    }
//}
