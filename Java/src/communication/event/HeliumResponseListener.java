package communication.event;

import communication.HeliumPackage;
import event.BlockingListener;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Aaron Nies on 5/26/2015.
 * Copyright 2015 all rights reserved.
 */
public class HeliumResponseListener<T extends Serializable> extends BlockingListener<HeliumPackage<T>>
{
    // --- Vars ---

    private final UUID _previous;
    private HeliumPackage<T> _response;

    // --- Constructor ---

    public HeliumResponseListener(HeliumPackage heMessage)
    {
        super();
        _previous = heMessage.getID();
    }

    // --- Methods ---

    @Override
    public synchronized void trigger(HeliumPackage<T> hePackage)
    {
        _response = hePackage;
        super.trigger(hePackage);
    }

    @Override
    public HeliumPackage<T> block(long timeout) throws InterruptedException
    {
        if (_response == null) // Only block if response has not been found.
        {
            super.block(timeout);
        }
        return _response;
    }

    @Override
    protected boolean filter(HeliumPackage<T> o)
    {
        boolean isResponse = false;
        if (super.filter(o)) // If HeliumPackage.
        {
            HeliumPackage hePackage = (HeliumPackage) o;
            if ( hePackage.isResponse() && hePackage.getPrevious() == _previous) // If flagged as response.
            {
                isResponse = true;
                try
                {
                    @SuppressWarnings("unchecked")
                    T cast = (T) hePackage.getContents(); // Checks if the return type is correct.
                }
                catch (ClassCastException e)
                {
                    isResponse = false;
                }
            }
        }
        return isResponse;
    }
}
