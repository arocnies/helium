package communication.event;

import event.Listener;

/**
 * Created by Aaron Nies on 6/10/2015.
 * Copyright 2015 all rights reserved.
 */
public abstract class Processor<T> extends Listener<T>
{
    @Override
    public void trigger(T t)
    {
        process(t);
    }

    public abstract T process(T t);
}
