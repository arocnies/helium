package server;

import communication.Transport;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Aaron Nies on 5/20/2015.
 * Copyright 2015 all rights reserved.
 */
public abstract class ServerSocketHandle implements Runnable, Transport
{

    // --- Vars ---

    private int _port;
    private ServerSocket _serverSocket;

    // --- Constructors ---

    public ServerSocketHandle(int port)
    {
        if (port < 1024) throw new IllegalArgumentException();
        _port = port;
    }

    // --- Methods ---

    protected void startListening()
    {
        try (ServerSocket serverSocket = new ServerSocket(_port))
        {
            _serverSocket = serverSocket;
            while (true) newConnection(serverSocket.accept());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    protected void stopListening()
    {
        if (_serverSocket != null)
        {
            try
            {
                _serverSocket.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        _serverSocket = null;
    }

    @Override
    public void run()
    {
        startListening();
    }

    public boolean isRunning()
    {
        return !_serverSocket.isClosed();
    }

    // --- Abstract ---

    protected abstract void newConnection(Socket socket);
}
