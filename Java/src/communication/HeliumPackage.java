package communication;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Aaron Nies on 5/21/2015.
 * Copyright 2015 all rights reserved.
 */
public class HeliumPackage<T extends Serializable> implements Serializable
{
    private final UUID _id;

    private final T _contents;
    private final UUID _previous;
    private final boolean _isRequest;
    private final boolean _isResponse;

    public HeliumPackage(T contents, HeliumPackage previous, boolean isRequest, boolean isResponse)
    {
        _id = UUID.randomUUID();
        _contents = contents;
        _previous = (previous == null ? null : previous.getID());
        _isRequest = isRequest;
        _isResponse = isResponse;
    }

    public HeliumPackage(T contents, HeliumPackage previous)
    {
        this(contents, previous, false, false);
    }

    public HeliumPackage(T contents)
    {
        this(contents, null, false, false);
    }

    public T getContents()
    {
        return _contents;
    }

    public UUID getPrevious()
    {
        return _previous;
    }

    public UUID getID()
    {
        return _id;
    }

    public boolean isRequest()
    {
        return _isRequest;
    }

    public boolean isResponse()
    {
        return _isResponse;
    }
}
